import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
const uuidAPIKey = require('uuid-apikey');



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  showPassword = false;
  username:string;
  password:string;
  apiData:any;
route:any
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  generateApiKey(){
    this.apiData = uuidAPIKey.create()
    console.log(this.apiData)
    
  }

  onSubmit() {
/*     console.log(uuidAPIKey.create())

 */
const data = {
  username:this.username,
  password:this.password
}
this.http.post(`${environment.apiUrl}/api/ssoVerifyAccount/verifyAccount/73RM89V-F4F40AC-PA3Y3RT-602WAX5`, data).subscribe(res => {
  console.log(res)

this.route = res
window.location.href = this.route.data.url})

  }

}
